//
//  RealmManager.swift
//  My University
//
//  Created by V-jet on 04/12/2018.
//  Copyright © 2018 V-jet. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmProvider {
    func createRealmObject() -> Object
    func getRealmObject() -> Object?
    init?(realmObject: Object)
}

class RealmManager {
    
    private struct Listener {
        weak var owner: AnyObject?
        var block: ListenerBlock?
    }
    
    typealias ListenerBlock = ()->()
    
    static let shared = RealmManager()
    
    private var listeners: [Listener] = []
    
    private init() {
        configure()
    }
    
    func addListeners(_ owner: AnyObject, _ block: @escaping ListenerBlock) {
        listeners.append(Listener(owner: owner, block: block))
    }
    
    func saveToDB(objects: [RealmProvider], completion: ((Bool) -> Void)?) {
        DispatchQueue.global().async {
            let realmObjects = objects.map { $0.createRealmObject() }

            self.save(objects: realmObjects, completion: { (isWrite) in
                DispatchQueue.main.async {
                    completion?(isWrite)
                    self.changeNotification()
                }
            })
        }
    }
    
    func deleteFromDB(objects: [RealmProvider], completion: ((Bool) -> Void)?) {
        DispatchQueue.global().async {
            let realmObjects = objects.compactMap { $0.getRealmObject() }
            
            self.delete(objects: realmObjects, completion: { (isDelete) in
                DispatchQueue.main.async {
                    completion?(isDelete)
                    self.changeNotification()
                }
            })
        }
    }

    func object<T: Object, U: RealmProvider>(type: T.Type, id: Any) -> U? {
        
        guard let primaryKey = T.primaryKey() else { return nil }
        
        do {
            let realm = try Realm()
            guard let first = realm.objects(type).filter("\(primaryKey) == %@", id).first else { return nil }
            
            return U(realmObject: first)
        } catch {}
        return nil
    }

    func objects<T: Object, U: RealmProvider>(type: T.Type) -> [U]? {
        
        do {
            let realm = try Realm()
            let objects = realm.objects(type)
            return objects.compactMap { return U(realmObject: $0) }
        } catch {}
        return nil
    }
    
    //MARK: - Private methods
    
    fileprivate func objectRealm<T: Object>(type: T.Type, id: Any) -> T? {
        
        guard let primaryKey = T.primaryKey() else { return nil }
        
        do {
            let realm = try Realm()
            return realm.objects(type).filter("\(primaryKey) == %@", id).first
        } catch {}
        return nil
    }

    private func objects<T: Object>(type: T.Type) -> Results<T>? {
        
        do {
            let realm = try Realm()
            return realm.objects(type)
        } catch {}
        return nil
    }
    
    private func save(objects: [Object], completion: ((Bool) -> ())) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(objects, update: true)
            }
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    private func delete(objects: [Object], completion: ((Bool) -> ())) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.delete(objects)
            }
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    private func changeNotification() {
        
        DispatchQueue.main.async {
            self.listeners = self.listeners.compactMap{$0.owner == nil ? nil : $0}
            self.listeners.forEach{$0.block?()}
        }
    }
}

extension RealmManager {
    
    private func configure() {
        
        do {
            
            let nextVersion = try schemaVersionAtURL(Realm.Configuration.defaultConfiguration.fileURL!) + 1
            let config = Realm.Configuration(schemaVersion: nextVersion)
            Realm.Configuration.defaultConfiguration = config
            
        } catch {
            
            print(error.localizedDescription)
        }
    }
}

//MARK: - RealmProvider methods for all models

extension UserModel {
    func getRealmObject() -> Object? {
        return RealmManager.shared.objectRealm(type: UserRM.self, id: id)
    }
}

extension InstitutionModel {
    func getRealmObject() -> Object? {
        return RealmManager.shared.objectRealm(type: InstitutionRM.self, id: id)
    }
}

extension FacultyModel {
    func getRealmObject() -> Object? {
        return RealmManager.shared.objectRealm(type: FacultyRM.self, id: id)
    }
}

extension DepartmentModel {
    func getRealmObject() -> Object? {
        return RealmManager.shared.objectRealm(type: DepartmentRM.self, id: id)
    }
}

extension CityModel {
    func getRealmObject() -> Object? {
        return RealmManager.shared.objectRealm(type: CityRM.self, id: id)
    }
}

extension LastUpdateModel {
    func getRealmObject() -> Object? {
        return RealmManager.shared.objectRealm(type: LastUpdateRM.self, id: type)
    }
}

extension AcademicDegreesModel {
    func getRealmObject() -> Object? {
        return RealmManager.shared.objectRealm(type: AcademicDegreesRM.self, id: id)
    }
}

